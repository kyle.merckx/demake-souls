import pygame
from walls import BlackWall, GreenWall
from pygame.locals import *


map1 = [1, 1, 2, 1, 1,
        1, 0, 0, 0, 1,
        1, 0, 0, 0, 1,
        1, 0, 0, 0, 1,
        1, 1, 0, 1, 1,
        1, 1, 0, 1, 1,
        1, 0, 0, 0, 1,
        1, 0, 0, 0, 1,
        1, 0, 0, 0, 1,
        1, 1, 1, 1, 1]


def build_map(screen, map1):
    x = 625 // 5
    y = 1250 // 10
    x_now = 0
    y_now = 0
    for index, item in enumerate(map1):
        if item > 0:
            if item == 1:
                wall = BlackWall(screen, x_now, y_now)
            elif item == 2:
                wall = GreenWall(screen, x_now, y_now)
            wall.build()
        x_now += x
        if (index + 1) % 5 == 0:
            y_now += y
            x_now = 0
