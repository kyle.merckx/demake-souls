import pygame


fire = [0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0]

class BaseObject():
    def __init__(self, surface, pos_x, pos_y) -> None:
        self.surface = surface
        self.x = pos_x
        self.y = pos_y

    def draw_object(surface, objects):
        x = surface.get_width() // 5
        y = surface.get_height() // 5
        x_now = 0
        y_now = 0
        for index, item in enumerate(objects):
            if item > 0:
                if item == 1:
                    pygame.draw.rect(surface, "orange", (x_now, y_now, 25, 25))
                elif item == 2:
                    pygame.draw.rect(surface, "red", (x_now, y_now, 25, 25))
            x_now += x
            if (index + 1) % 5 == 0:
                y_now += y
                x_now = 0


class Fire(BaseObject):
    def __init__(self, surface, pos_x, pos_y):
        print(surface)
        super().__init__(surface, pos_x, pos_y)
        self.draw_object(surface, fire)
