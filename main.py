import pygame
from pygame.locals import *
from map import map1, build_map
from objects import Fire

#tile size is 125, 125
pygame.init()
screen = pygame.display.set_mode((625, 625))

title = "Demake Souls"
pygame.display.set_caption(title)
running = True
FPS = pygame.time.Clock()
dt = 0


#screen stuff
center = pygame.Vector2(screen.get_width() / 2, screen.get_height() / 2)
player_pos = pygame.Vector2(center.x, center.y)
floor = pygame.Surface((700, 700))
floor.fill("blue")


#audio
# pygame.mixer.init(frequency=44100,
#                   size=-16,
#                   channels=2,
#                   buffer=512,
#                   devicename=None,
#                   allowedchanges=AUDIO_ALLOW_FREQUENCY_CHANGE | AUDIO_ALLOW_CHANNELS_CHANGE)
# pygame.mixer.set_num_channels(2)
# sound = pygame.mixer.Sound("DeadFish.wav")
# pygame.mixer.Sound.play(sound)


while running:
    # poll for events
    # pygame.QUIT event means the user clicked X to close your window
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # fill the screen with a color to wipe away anything from last frame
    screen.fill("blue")
    screen.blit(floor, (0, 0))
    build_map(floor, map1)
    player = pygame.image.load("Untitled.png")
    floor.blit(player, (center.x - 60, center.y - 60))
    fire = pygame.draw.circle(floor, "orange", (125, 375), 35)
    fire1 = pygame.draw.circle(floor, "red", (125, 375), 25)
    # RENDER YOUR GAME HERE
    keys = pygame.key.get_pressed()
    if keys[pygame.K_s]:
        floor.scroll(0, -5)
    if keys[pygame.K_w]:
        floor.scroll(0, 5)
    if keys[pygame.K_d]:
        floor.scroll(-5, 0)
    if keys[pygame.K_a]:
        floor.scroll(5, 0)

    if keys[pygame.K_SPACE]:
        pygame.quit()



    # flip() the display to put your work on screen

    pygame.display.update()

    dt = FPS.tick(60) / 1000  # limits FPS to 60

pygame.quit()
