import pygame


class Block():
    def __init__(self, surface, color, pos_x, pos_y) -> None:
        self.surface = surface
        self.color = color
        self.x = pos_x
        self.y = pos_y

    def build(self):
        pygame.draw.rect(self.surface, self.color, (self.x, self.y, 125, 125))


class BlackWall(Block):
    def __init__(self, surface, pos_x, pos_y):
        super().__init__(surface, "black", pos_x, pos_y)


class GreenWall(Block):
    def __init__(self, surface, pos_x, pos_y):
        super().__init__(surface, "aqua", pos_x, pos_y)
